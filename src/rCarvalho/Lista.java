package rCarvalho;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Lista implements List {
    private Object[] list;
    private Integer trackIndex;

    public Lista(int max){
        this.list = new Object[max];
        this.trackIndex = 0;
    }
    @Override
    public int size() {
        return this.trackIndex;
    }

    @Override
    public boolean isEmpty() {
        if(trackIndex == 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean contains(Object o) {
        if(indexOf(o) > -1){
            return true;
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[size()];
        System.arraycopy(list, 0, array, 0, size());
        return array;
    }

    @Override
    public boolean add(Object o) {
        list[trackIndex] = o;
        trackIndex++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        for (int i = 0; i < size(); i++) {
            if(list[i] == o) {
                list[i] = null;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        return false;
    }

    @Override
    public void clear() {
        toArray();
        trackIndex = 0;
    }

    @Override
    public Object get(int index) {
        if(list[index] != null){
            return list[index];
        }
        return null;
    }

    @Override
    public Object set(int index, Object element) {
        if(list[index] == null){
            list[index] = element;
            return element;
        }
        return null;
    }

    @Override
    public void add(int index, Object element) {
        if(trackIndex <= size() ){
            set(index, element );
            trackIndex++;
        } else {
            System.out.println( "List is full" );
        }
    }

    @Override
    public Object remove(int index) {
        if(list[index] != null) {
            list[index] = null;
        }
        return list;
    }

    @Override
    public int indexOf(Object o) {
        for (int i = 0; i < size(); i++){
            if(list[i] == o) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        Integer lastIndex = 0;
        if(indexOf(o) > -1){
            for (int i = 0; i < size(); i++) {
                if(list[i] == o && i != lastIndex) {
                    lastIndex = i;
                }
            }
            return lastIndex;
        }

        return -1;
    }

    @Override
    public ListIterator listIterator() {
        return null;
    }

    @Override
    public ListIterator listIterator(int index) {
        return null;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        Lista out = new Lista(toIndex - fromIndex + 1);
        if(fromIndex < 0 || toIndex > size() ) {
            return null;
        }
        for (int i = fromIndex; i <= toIndex; i++) {
            out.add(list[i]);
        }
        return out;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }
}

